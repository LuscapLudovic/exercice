use std::io;
use std::io::Write;
use std::str::FromStr;

pub mod calcul;
pub mod product;

fn main() {
    loop {
        println!("1 - Calculé Montant TTC");
        println!("2 - Pays TVA");
        println!("3 - Afficher les réductions");
        println!("4 - quitter le programme");
        match read_usize("Indiquer un nombre : ") {
            1 => {
                println!("Montant TTC : {}", calcul::get_montant_ttc())
            }
            2 => {
                calcul::pays_tva();
            }
            3 => {
                calcul::print_reductions();
            }
            4 => {
                println!("fin de l'execution");
                break;
            }
            _ => {
                println!("Ce nombre ne correspond à aucun menu !");
            }
        }
    }
}

fn read_string(message: &str) -> String {
    let mut input = String::new();
    loop {
        print!("{}", message);
        let _ = io::stdout().flush();
        match io::stdin().read_line(&mut input) {
            Ok(_) => return input.trim().to_string(),
            _ => println!("Erreur lors de la récupération de la saisie ...")
        }
    }
}

fn read_f32(message: &str) -> f32 {
    loop {
        match f32::from_str(&read_string(message)) {
            Ok(nb) => return nb,
            Err(_) => println!("Veuillez entrer un nombre valide !")
        }
    }
}

fn read_usize(message: &str) -> usize {
    loop {
        match usize::from_str(&read_string(message)) {
            Ok(nb) => return nb,
            Err(_) => println!("Veuillez entrer un nombre valide !")
        }
    }
}
