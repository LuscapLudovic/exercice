use std::collections::HashMap;
use crate::{read_f32, read_usize};
use crate::product;

pub fn get_montant_ht() -> f32 {
    let mut products: Vec<product::Product> = vec![];
    loop {
        println!("1 - Ajouter un produit");
        println!("2 - Valider");
        match read_usize("Indiquer un nombre : ") {
            1 => products.push(product::get_product()),
            2 => {
                return products.iter().map(|p| p.clone().get_value_ht()).sum()
            }
            _ => println!("Ce nombre ne correspond à aucune action !")
        }
    }
}

fn get_montant_tva() -> f32 {
    loop {
        let nb = read_f32("Indiquer le montant de la TVA : ");
        if nb >= 0.0 && nb <= 1.0 {
            return nb
        }
        println!("La TVA doit étre compris entre 0 et 1")
    }
}

pub(crate) fn get_montant_ttc() -> f32 {
    let mut total_ht = get_montant_ht();
    recommend_reduction(total_ht);
    total_ht -= add_reduction_for_user();
    return total_ht * (1.0 - get_montant_tva())
}

fn add_reduction_for_user() -> f32 {
    read_f32("Veuillez saisir le montant de la réduction : ")
}

fn recommend_reduction(montant : f32) {
    let reductions = [(3, 1000), (5, 5000), (7, 7000), (10, 10000), (15, 50000)];
    let mut act_reduction: (i32, i32) = (0, 0);
    for reduction in reductions.iter() {
        if montant >= reduction.1 as f32 { act_reduction = reduction.clone() }
    }
    if act_reduction != (0,0) { println!("Ayant un montant supérieur à {}€, nous vous conseillons un réduction de {}%", act_reduction.1, act_reduction.0) }
}

pub fn print_reductions() {
    println!("les reductions actuelles : ");
    let reductions = [(3, 1000), (5, 5000), (7, 7000), (10, 10000), (15, 50000)];
    for reduction in reductions.iter() {
        println!("une réduction de {}%, a partir de {}€", reduction.0, reduction.1);
    }
}

pub fn pays_tva() {
    let pays = vec!["FR", "BE", "ES", "DE", "DN", "IT"];
    let tva = vec![0.2, 0.21, 0.21, 0.19, 0.25, 0.22];
    let mut tva_pays = HashMap::new();
    tva_pays.insert(pays, tva);

    for (key, value) in &tva_pays {
        println!("{:?}: {:?}", key, value);
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_pays_tva(){
        assert_eq!(pays_tva(), ());
    }

    #[test]
    fn test_print_reductions(){
        assert_eq!(print_reductions(), ());
    }
}
