use crate::{read_usize, read_f32, read_string};

#[derive(Debug, Clone)]
pub struct Product {
    name: String,
    quantite: usize,
    montant_ht: f32,
}

impl Product {
    pub fn new(name: String, quantite: usize, montant_ht: f32) -> Self {
        Self {
            name: name.clone(),
            quantite,
            montant_ht,
        }
    }

    pub fn get_value_ht(self) -> f32 {
        self.quantite as f32 * self.montant_ht
    }
}

pub fn get_product() -> Product {
    println!("Renseger les information du produit, ci-desous :");
    return Product::new(
        read_string("    |-> Nom : "),
        read_usize("    |-> Quantité : "),
        read_f32("    |-> Montant HT : ")
    )
}
